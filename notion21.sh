#!/bin/bash

# 脚本名称: deploy_notionnext.sh
# 描述: 自动部署NotionNext到VPS

# 定义变量
NOTIONNEXT_DIR="/path/to/notionnext"  # 替换为您的NotionNext目录
NODE_VERSION="v18.17.0"  # 替换为您想安装的NodeJS版本

# 更新系统并安装必要的工具
echo "更新系统并安装必要的工具..."
sudo yum update -y
sudo yum install git -y

# 下载NotionNext代码
echo "下载NotionNext代码..."
git clone https://github.com/tangly1024/NotionNext $NOTIONNEXT_DIR
cd $NOTIONNEXT_DIR

# 安装nvm
echo "安装nvm..."
git clone https://github.com/cnpm/nvm.git ~/.nvm && cd ~/.nvm && git checkout `git describe --abbrev=0 --tags`
echo "source ~/.nvm/nvm.sh" >> ~/.bashrc
source ~/.bashrc

# 使用nvm安装NodeJS
echo "使用nvm安装NodeJS $NODE_VERSION..."
nvm install $NODE_VERSION
nvm use $NODE_VERSION

# 安装yarn
echo "安装yarn..."
npm install -g yarn

# 安装项目依赖
echo "安装项目依赖..."
yarn

# 编译项目
echo "编译项目..."
yarn build

# 设置环境变量
echo "设置环境变量..."
export NEXT_PUBLIC_VERSION=V4.6.1
export NOTION_PAGE_ID=1de3465160bf464e8536830a075de4b6
export NEXT_PUBLIC_THEME=heo
export NEXT_PUBLIC_WIDGET_PET_LINK=https://cdn.jsdelivr.net/npm/live2d-widget-model-hijiki/assets/hijiki.model.json
export NEXT_PUBLIC_CUSTOM_MENU=true
export NEXT_PUBLIC_PRISM_THEME_PREFIX_PATH=https://npm.elemecdn.com/prism-themes/themes/prism-holi-theme.css
export NEXT_PUBLIC_CODE_MAC_BAR=true

# 启动项目
echo "启动项目..."
nohup yarn start >/dev/null 2>&1 &

echo "部署完成！"